data "template_file" "init" {
  template = file("${path.module}/init.sh.tpl")

  vars = {
    name = var.name
    port = var.port
  }
}

resource "aws_instance" "default" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_size
  key_name                    = var.ssh_key_name
  subnet_id                   = var.subnet_id
  associate_public_ip_address = var.pub_ip
  security_groups             = [aws_security_group.default.id]
  lifecycle {
    ignore_changes = [
      security_groups,
    ]
  }
  user_data = data.template_file.init.rendered
  tags = {
    Name = var.name
  }
}

resource "aws_security_group" "default" {
  name        = format("%s-sg", var.name)
  description = format("Security group for the %s tinyproxy instance", var.name)
  vpc_id      = var.vpc_id

  ingress {
    description = "Proxy traffic"
    from_port   = var.port
    to_port     = var.port
    protocol    = "tcp"
    cidr_blocks = [
      "10.0.0.0/8",
      "192.168.0.0/16",
      "172.16.0.0/12",
    ]
  }

  ingress {
    description = "SSH Management"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      var.management
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "default" {
  count    = var.eip ? 1 : 0
  instance = aws_instance.default.id
  vpc      = true
}
