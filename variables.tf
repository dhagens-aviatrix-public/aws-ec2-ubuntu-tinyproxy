variable "name" {
  type = string
}

variable "ubuntu_image" {
  type    = string
  default = "ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"
}

variable "subnet_id" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "ssh_key_name" {
  type = string
}

variable "instance_size" {
  type    = string
  default = "t3.micro"
}

variable "pub_ip" {
  type    = bool
  default = true
}

variable "eip" {
  type    = bool
  default = false
}

variable "management" {
  default = "0.0.0.0/0"
}

variable "port" {
  default = 3128
}
