# aws-ec2-ubuntu-tinyproxy

This module deploys a Ubuntu instance (latest) in AWS and installs TinyProxy. This module was developed for testing and demo purposes only.

The following variables are required:

key | value
--- | ---
name | Name of the Ubuntu instance
ssh_key_name | Key to insert into Ubuntu machine
subnet_id | Subnet ID in which the host will be launched
vpc_id | VPC ID in which the host will be launched

The following variables are optional:

key | default | value
--- | --- | ---
eip | false | assigns an EIP hen set to true.
instance_size | t3.micro | Ubuntu instance size
management | "0.0.0.0/0" | CIDR from which SSH management to this instance is allowed.
port | 3128 | TCP port proxy server listens to.
pub_ip | true | Set to true to enable public IP allocation
ubuntu_image | ubuntu/images/hvm-ssd/ubuntu-focal-22.04-amd64-server-* | string to search for Ubuntu image

Example
```hcl
module "tinyproxy1" {
  source = "git::https://gitlab.com/dhagens-aviatrix-public/aws-ec2-ubuntu-tinyproxy"

  name         = "tinyproxy1"
  vpc_id       = "vpc-12344556"
  subnet_id    = "subnet-1234567"
  ssh_key_name = "MyKey"
}
```