#! /bin/bash
sudo hostnamectl set-hostname ${name}
sudo apt update
sudo apt -y install tinyproxy tinyproxy-bin
sudo echo "Allow 10.0.0.0/8" >> /etc/tinyproxy/tinyproxy.conf
sudo echo "Allow 192.168.0.0/16" >> /etc/tinyproxy/tinyproxy.conf
sudo echo "Allow 172.16.0.0/12" >> /etc/tinyproxy/tinyproxy.conf
sudo sed -i 's/8888/${port}/' /etc/tinyproxy/tinyproxy.conf
sudo systemctl enable tinyproxy
sudo systemctl restart tinyproxy
